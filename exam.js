
//Name and Surname of users without 2ND surname and living at Spain.
db.someorders.find( {$and : [{"Sec_Surname":{$not:{$exists:true}}}, 
	{"Address.Country":"Spain"} ]}, {"Name":1,"Surname":1,"_id":0} )

//Count 1

//Name and Surname(s) of clients with single-purchase orders
//(at least one order with just one order line)
db.someorders.find( {"Orders": {$elemMatch: {"Lines": {$size: 1}}}} , 
	{"Name":1, "Surname":1, "Sec_Surname":1 ,"_id":0})

//count: 2573

//Surnames ending with ‘ez’
db.someorders.find( {$or: [{"Surname": {$regex: /.*(ez$)/}}, 
							{"Sec_Surname":{$regex:/.*(ez$)/}}] }, 
	{"Surname":1, "Sec_Surname":1, "_id":0} )

//Count: 498

//Username(s) of users with valid password (a string, with at least one char).
db.someorders.find( {$and:[ {"Password":{$type:"string"}}, 
							{"Password":{$not:{$size:0}}} ]}, 
	{"Username":1, "_id":0})

//Count: 1915

//1% of coffee sales goes to the producer. Obtain total earnings per
//country (Origin).

 db.someorders.aggregate( {$group: {_id:"$Address.Country", mycard:{$sum:1}, mysum:{$sum:"$Orders.Lines.Retail_price"}  } }  )


var countOrders = function(){
	for (var i=0, len = this.Orders.length; i < len; i++){
		var Order = this.Orders[i];
		for (var j=0, lenj = Order.Lines.length; j < lenj; j++){
			emit(Order.Lines[j].Origin, Order.Lines[j].Retail_price * Order.Lines[j].Quantity*(0.01))
		}
	}
}

var reduceOrders = function(key, query){
	return (Array.sum(query))
}

 db.someorders.mapReduce(countOrders, reduceOrders, {out:"total"} )

 db.total.find() //see the results